function changeForm() {
  document.querySelector(".form-wrap.active").classList.remove("active");
  if (this.closest(".form-wrap").id === "form1") {
    document.querySelector("#form2").classList.add("active");
  } else {
    document.querySelector("#form1").classList.add("active");
  }
}

document.addEventListener("DOMContentLoaded", () => {
  let form1 = document.querySelector("#form1");
  let form2 = document.querySelector("#form2");

  form1.querySelector(".form-switch").onclick = form2.querySelector(
    ".form-switch"
  ).onclick = changeForm;

  let loginSubmit = document.querySelector("#loginSubmit");
  loginSubmit.addEventListener("click", (event) => {
    event.preventDefault();

    let loginEmail = document.querySelector("#loginEmail");
    let loginPass = document.querySelector("#loginPass");
    let loginEmailError = document.querySelector("#loginEmailError");
    let loginPassError = document.querySelector("#loginPassError");
    let labelEmail = document.querySelector("#labelEmail");
    let labelPass = document.querySelector("#labelPass");
    let loginUpdates = document.querySelector("#loginUpdates");
    let loginUpdatesError = document.querySelector("#loginUpdatesError");

    loginEmailError.textContent = "";
    loginPassError.textContent = "";
    loginUpdatesError.textContent = "";
    loginEmail.classList.remove("field-error");
    loginPass.classList.remove("field-error");
    labelEmail.classList.remove("field-error");
    labelPass.classList.remove("field-error");
    loginUpdatesError.classList.remove("field-error");

    if (loginEmail.value === "") {
      loginEmail.classList.add("field-error");
      labelEmail.classList.add("field-error");
      loginEmailError.style.visibility = "visible";
      loginEmailError.textContent = "Email обязателен для заполнения.";
    }
    if (loginPass.value === "") {
      loginPass.classList.add("field-error");
      labelPass.classList.add("field-error");
      loginPassError.style.visibility = "visible";
      loginPassError.textContent = "Пароль обязателен для заполнения.";
    }
    if (!loginUpdates.checked) {
      loginUpdatesError.style.visibility = "visible";
      loginUpdatesError.textContent = "Поле обязательно для заполнения";
    }

    if (
      (loginEmail.value !== "" && loginPass.value !== "") ||
      loginUpdates.checked
    ) {
      let users = JSON.parse(localStorage.getItem("users")) || [];
      let user = users.find(
        (user) =>
          user.email === loginEmail.value && user.password === loginPass.value
      );

      if (user) {
        window.location.href = "главная.html";
      } else {
        loginPassError.textContent = "Неправильный email или пароль";
      }
    }
  });

  let registrationSubmit = document.querySelector("#registrationSubmit");
  registrationSubmit.addEventListener("click", (event) => {
    event.preventDefault();
    const emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    let registrationEmail = document.querySelector("#registrationEmail");
    let registrationPass = document.querySelector("#registrationPass");
    let registrationEmailError = document.querySelector(
      "#registrationEmailError"
    );
    let registrationUpdates = document.querySelector("#registrationUpdates");
    let registrationPassError = document.querySelector(
      "#registrationPassError"
    );
    let registrationUpdatesError = document.querySelector(
      "#registrationUpdatesError"
    );
    let labelEmail = document.querySelector("#labeEmailReg");
    let labelPass = document.querySelector("#labelPassReg");

    registrationEmailError.textContent = "";
    registrationPassError.textContent = "";
    registrationEmail.classList.remove("field-error");
    registrationPass.classList.remove("field-error");
    registrationUpdates.classList.remove("field-error");
    labelEmail.classList.remove("field-error");
    labelPass.classList.remove("field-error");
    if (registrationEmail.value === "") {
      labelEmail.classList.add("field-error");
      registrationEmail.classList.add("field-error");
      registrationEmailError.style.visibility = "visible";
      registrationEmailError.textContent = "Email обязателен для заполнения.";
    }
    if (registrationPass.value.trim() === "") {
      labelPass.classList.add("field-error");
      registrationPass.classList.add("field-error");
      registrationPassError.style.visibility = "visible";
      registrationPassError.textContent = "Пароль обязателен для заполнения.";
    } else if (registrationPass.value.length < 8) {
      registrationPassError.textContent = "Пароль меньше 8 символов";
    }
    if (!registrationUpdates.checked) {
      registrationUpdates.classList.add("field-error");
      registrationUpdatesError.style.visibility = "visible";
      registrationUpdatesError.textContent = "Поле обязательно для заполнения.";
    }
    if (!emailRegex.test(registrationEmail.value)) {
      registrationEmailError.style.visibility = "visible";
      registrationEmailError.textContent = "Email невалидный";
      return;
    }
    if (registrationEmail.value !== "" && registrationPass.value !== "") {
      let users = JSON.parse(localStorage.getItem("users")) || [];
      let checkUser = users.some(
        (user) => user.email === registrationEmail.value
      );

      if (checkUser) {
        registrationEmailError.textContent =
          "Пользователь с таким email уже существует";
      } else {
        users.push({
          email: registrationEmail.value,
          password: registrationPass.value,
        });
        localStorage.setItem("users", JSON.stringify(users));

        registrationEmailError.textContent = "";
        registrationPassError.textContent = "";
        alert("Регистрация успешна! Теперь вы можете войти в систему.");
      }
    }
  });
});
